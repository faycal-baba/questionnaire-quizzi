import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ReceptionComponent } from './reception/reception.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardService } from './auth-guard.service';
import { CreateComponent } from './profile/create/create.component';
import { AnswerComponent } from './answer/answer.component';

const routes: Routes = [
  { path: 'reception', component: ReceptionComponent },
  { path: '', redirectTo: 'reception', pathMatch: 'full' },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path: 'answer/:id', component: AnswerComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
