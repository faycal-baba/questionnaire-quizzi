let icons: any = {
	alert: '',
	error: '',
	info: '',
	success: "<i class='fa fa-check' aria-hidden='true'></i>",
	warn: ""
};

export class NotificationConstants {

	/* ADMIN */

	public static get SUCCESS_MESSAGE(): string { return 'SUCCESS !!!'; }


	/* ERRORS */
	public static get ERROR_MESSAGE(): string { return 'FAILURE !!!'; }



	// tslint:disable-next-line:indent
	public static get PROPERTIES(): any {
		return {
			timeOut: 3000,
			showProgressBar: false,
			pauseOnHover: false,
			clickToClose: true,
			rtl: false,
			animate: 'fromBottom',
			icons: icons,
		};
	}

}
