import { ReceptionRoutingModule } from './reception-routing.module';

describe('ReceptionRoutingModule', () => {
  let receptionRoutingModule: ReceptionRoutingModule;

  beforeEach(() => {
    receptionRoutingModule = new ReceptionRoutingModule();
  });

  it('should create an instance', () => {
    expect(receptionRoutingModule).toBeTruthy();
  });
});
