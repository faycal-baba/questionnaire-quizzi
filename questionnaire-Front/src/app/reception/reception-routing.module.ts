import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';


import { ReceptionComponent } from './reception.component';
const routes: Routes = [
  {
    path: 'reception',
    component: ReceptionComponent
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class ReceptionRoutingModule { }
