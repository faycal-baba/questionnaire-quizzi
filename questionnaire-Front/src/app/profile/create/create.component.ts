import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

import { QuestionService } from 'src/services/question.service';
import { QuestionnaireService } from 'src/services/questionnaire.service';
import { Questionnaire, User, Question } from 'src/models/models';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  @Input() questions: Question[] = [];
  @Input() questionnaire: Questionnaire = new Questionnaire();
  @Input() user: User;
  @Input() inUpdate: Boolean = false;
  @Output() createFinish: EventEmitter<Questionnaire> = new EventEmitter<Questionnaire>();
  @Output() updateFinish: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  constructor(private questionService: QuestionService,
    private questionnaireService: QuestionnaireService, private router: Router) { }

  ngOnInit() {
  }

  addQuestion() {
    this.questionService
      .addQuestion()
      .subscribe(result => result && this.questions.push(result));
  }

  updateQuestionnaire() {
    // console.log("create/update()");

    this.questionnaireService.udapte(this.questionnaire, this.questions).subscribe(res => {
      // console.log(res);
      this.updateFinish.emit(false);
    }, (err) => {
      console.log(err);
    });
    this.router.navigateByUrl("profile");

  }

  save() {
    // console.log('save ' + this.questionnaire.title + this.questionnaire.description + this.questions.toString);
    // console.log(this.user.email);
    let questionnaire = new Questionnaire(
      this.questionnaire.title,
      this.questionnaire.description, this.user.name, this.user.email, this.questions, this.questionnaire.dateLimite);
    this.questionnaireService
      .create(questionnaire)
      .subscribe(res => {
        console.log(res);
        this.createFinish.emit(res);
      }, (err) => {
        console.log(err);
      });
  }

  quitter() {
    this.createFinish.emit(null);
    this.updateFinish.emit(false);
  }
  public captureScreen() {
    let data = document.getElementById('elementToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      let imgWidth = 208;
      let pageHeight = 295;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png');
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      let position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
  }


}
