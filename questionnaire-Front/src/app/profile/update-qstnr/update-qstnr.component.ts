import { Component, OnInit, Input } from '@angular/core';
import { Questionnaire, Question } from 'src/models/models';
import { Observable } from 'rxjs';
import { QuestionnaireService } from 'src/services/questionnaire.service';
import { QuestionService } from 'src/services/question.service';

@Component({
  selector: 'app-update-qstnr',
  templateUrl: './update-qstnr.component.html',
  styleUrls: ['./update-qstnr.component.css']
})
export class UpdateQstnrComponent implements OnInit {

  @Input() questionnaireToUpdate: Questionnaire;
  // questions: Observable<any[]>;
  listeQuestions: Question[];

  constructor(private questionService: QuestionService,
    private questionnaireService: QuestionnaireService) { }

  ngOnInit() {
    this.questionService.getListQuestions(this.questionnaireToUpdate._id).subscribe(questions => {
     // console.log(questions);
      // this.questions = questions;
      this.listeQuestions = questions;
    }, err => {
      console.log(err);
    });
  }

  addQuestion() {
    this.questionService
      .addQuestion()
      .subscribe(result => result && this.listeQuestions.push(result));
  }

  save() {
    // console.log(this.questionnaireToUpdate.dateCreate);

  }
}
