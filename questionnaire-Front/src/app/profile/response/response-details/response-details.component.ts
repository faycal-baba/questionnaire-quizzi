import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Question, Questionnaire } from 'src/models/models';

@Component({
  selector: 'app-response-details',
  templateUrl: './response-details.component.html',
  styleUrls: ['./response-details.component.css']
})
export class ResponseDetailsComponent implements OnInit {

  @Input() questionnaire: Questionnaire;
  @Input() answers: [any];
  questions: Question[] = [];
  @Output() toResponsePgae: EventEmitter<Boolean> = new EventEmitter<Boolean>();
  constructor() { }

  ngOnInit() {
    if (this.answers) {
      this.answers.forEach(element => {
        let q = new Question(element.title, element.type, []);
        // console.log("onInit/response details -- answer" + JSON.stringify(element.answer));
        switch (element.type) {
          case 'BOOLEAN':
            q.values.push({ "value": element.answer });
            break;
          case 'MULTI_CHOICE_MULTI':
            element.answer.forEach(val => {
              // console.log("-----------------" + JSON.stringify(val));
              q.values.push(val);
            });
            break;
          case 'MULTI_CHOICE_SINGLE':
            // console.log("-----------------" + JSON.stringify(element.answer));
            q.values.push(element.answer);
            break;
          case 'LONG_ANSWER':
            q.answer = element.answer;
            break;
          case 'SHORT_ANSWER':
            q.answer = element.answer;
            break;
          case 'NUMERICAL':
            q.answer = element.answer;
            break;
          case 'DATE_TYPE':
            q.answer = element.answer;
            break;
        }
        // console.log("onInit/response details my question" + JSON.stringify(q));
        this.questions.push(q);
      });
    }
  }

  goToResponse() {
    this.toResponsePgae.emit(false);
  }

}
