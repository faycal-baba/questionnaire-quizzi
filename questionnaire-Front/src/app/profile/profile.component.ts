import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ReceptionService } from '../reception/reception.service';
import { QuestionnaireService } from 'src/services/questionnaire.service';
import { QuestionService } from 'src/services/question.service';
import { NotificationsService } from 'angular2-notifications';
import { NotificationConstants } from '../utils/notification.constants';

import { User, Questionnaire, EtatQuestionnaire, Question } from 'src/models/models';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile: User;
  listeQuestionnaire: Questionnaire[];
  create: Boolean; // si true => charger la page de création de questionnaire
  update: Boolean; // si true => charger la page de modification de questionnaire
  viewResponse: Boolean;
  timerspiner = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  questionnaireToupdate: Questionnaire;
  listeQuestions: Question[];

  constructor(private receptionService: ReceptionService,
    private questionnaireService: QuestionnaireService,
    private questionService: QuestionService,
    private notificationsService: NotificationsService) {
  }

  ngOnInit() {
    this.create = false;
    this.update = false;
    this.viewResponse = false;
    this.receptionService.profile().subscribe(user => {
      this.profile = user;
      this.questionnaireService.getList().subscribe(questionaires => {
        // console.log(questionaires);
        this.listeQuestionnaire = questionaires.filter(e => e.submittedById == this.profile.email);

      }, err => {
        console.log(err);
      });
    }, err => {
      console.error(err);
    });
  }

  createFinish(newQuestionnaire: Questionnaire) {
    this.create = false;
    if (newQuestionnaire) {
      this.listeQuestionnaire.push(newQuestionnaire);
    }
  }

  updateFinish(event: Boolean) {
    this.update = event;
  }

  public goToCreate() {
    this.create = true;
  }

  public removeQstnr(questionnaireId) {
    // console.log('removeQstnr/remove ' + questionnaireId);

    this.questionnaireService.remove(questionnaireId).subscribe(q => {
      this.listeQuestionnaire = this.listeQuestionnaire.filter((qstnr: Questionnaire) => qstnr._id !== questionnaireId
      );
    });

  }

  public goToUpdate(questionnaire: Questionnaire) {
    /*if (questionnaire.etat === EtatQuestionnaire["En cours"]) { // si le questionnaire est publie pas de modification
      this.update = true;
    }
    else {
      // ouvrire boite de dialog pour information // modification pas possible
    }
    */
    this.update = true;
    this.questionnaireToupdate = questionnaire;
    this.questionService.getListQuestions(this.questionnaireToupdate._id).subscribe(questions => {
      // console.log(questions);
      // this.questions = questions;
      this.listeQuestions = questions;
    }, err => {
      console.log(err);
    });
  }

  goToResponse(questionnaire: Questionnaire) {
    this.viewResponse = true;
    this.questionnaireToupdate = questionnaire;
    // console.log("goToResponse");

  }

  generateUrl(questionaireId): void {
    let url = 'http://localhost:4200/answer/' + questionaireId;
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    document.body.appendChild(selBox);
    selBox.value = url;
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.notificationsService.success('Url copiée dans le presse papier', '', NotificationConstants.PROPERTIES);
  }

  goToProfile(value: Boolean) {
    this.viewResponse = false;
  }

}
