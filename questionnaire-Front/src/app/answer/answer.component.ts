import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionnaireService } from 'src/services/questionnaire.service';
import { Questionnaire } from 'src/models/models';
import { ReceptionService } from '../reception/reception.service';
import { QuestionService } from 'src/services/question.service';
import { AnswerService } from 'src/services/answer.service';
import { NotificationsService } from 'angular2-notifications';
import { NotificationConstants } from '../utils/notification.constants';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  public questionnaire: Questionnaire;
  public questionsList: Observable<any>;
  private router: any;

  constructor(private route: ActivatedRoute, private questionService: QuestionService,
    private questionnaireService: QuestionnaireService,
    private answerService: AnswerService,
    private receptionService: ReceptionService,
    private notificationService: NotificationsService,
    private routes: Router) { }

  ngOnInit() {
    this.router = this.route.params.subscribe(params => {
      let id = params['id'];
      // console.log("oninit" + id);

      if (id) {
        this.questionnaireService.get(id)
          .subscribe(response => {
            this.questionnaire = response;
            // console.log("questionnaire : " + JSON.stringify(response));

          });
        this.questionService.getListQuestions(id).subscribe(
          questionList => {
            this.questionsList = questionList;
          }
        );
        // console.log("questionnaire : " + this.questionsList);
      }
    });

  }

  submit(_questions) {
    let email = (this.receptionService.isLoggedIn()) ? this.receptionService.getUserDetails().email : 'Anonyme';
    let questions = [];
    _questions.forEach(question => {
      // console.log("question ++++" + JSON.stringify(question));

      let value = {
        _id: question._id,
        title: question.title,
        type: question.type,
        answer: question.answer
      };
      if (question.type === 'MULTI_CHOICE_MULTI') { value.answer = question.values; }
      if (question.type === 'MULTI_CHOICE_SINGLE') {
        value.answer = { "value": question.answer, "answer": question.answer };
        // console.log("submit answer " + JSON.stringify(value.answer));
      }
      questions.push(value);
    });
    // console.log(questions);
    this.answerService.submit(this.questionnaire._id, questions, email).subscribe(
      answer => {
        this.notificationService.success(NotificationConstants.SUCCESS_MESSAGE, '', NotificationConstants.PROPERTIES);
        this.routes.navigateByUrl('/profile');
      }, (err) => {
        console.log(err);
        this.notificationService.error(NotificationConstants.ERROR_MESSAGE, '', NotificationConstants.PROPERTIES);
      }
    );
  }
  ngOnDestroy() {
    this.router.unsubscribe();
  }

}
