import { Component, OnInit, Input } from '@angular/core';

import { Question } from 'src/models/models';
import { DialogsService } from 'src/services/dialogs';
import { QuestionnaireService } from 'src/services/questionnaire.service';
import { QuestionService } from 'src/services/question.service';


@Component({
  selector: 'question-type',
  templateUrl: './question-type.component.html',
  styleUrls: ['./question-type.component.less']
})
export class QuestionTypeComponent implements OnInit {

  @Input() questionnaireId: any;
  @Input() question: Question;
  @Input() qId: any;
  @Input() readonly: boolean;
  @Input() edit: boolean;
  @Input() removable: Boolean = true;

  public answer: string;

  constructor(private dialogsService: DialogsService, private questionnaireService: QuestionnaireService,
    public questionService: QuestionService) { }

  ngOnInit() {
    // console.log("questionvalues " + JSON.stringify(this.question));
    if (['BOOLEAN', 'MULTI_CHOICE_MULTI'].includes(this.question.type)) {

      if (this.question.values) {
        this.question.values = this.question.values.filter(q => !!q.value);
      }
    }
  }

  editQuestion(event) {
    event.preventDefault();

    this.questionService.editQuestion(this.question).subscribe(result => {
      if (result) {
        Object.assign(this.question, result);
        this.questionnaireService.udateQuestion(this.questionnaireId, this.qId, this.question);
      }
    });
  }

  removeQuestion(event) {
    event.preventDefault();

    this.dialogsService
      .confirm('Confirm Delete', 'Are you sure you want delete question?')
      .subscribe(res => res && this.questionnaireService.removeQuestion(this.questionnaireId, this.qId));
  }

}
