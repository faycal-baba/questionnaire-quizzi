const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let Question = new Schema({
  title: {
    type: String,
    trim: true,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  values: Array
});

Question.statics.updateQuestion = function
  updateQuestion(data) {
  this.update({
    _id: data._id
  }, data, function (error, res) {
    if (error) {
      res.send('Erreur pendant la modification de la question');
    } 
  });
}

var question = mongoose.model('Question', Question);

question.schema.path('title').validate(function (value) {
  return value.length <= 400;
}, 'Texte de la question trop long !');
module.exports = question;