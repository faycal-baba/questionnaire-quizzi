const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  shortId = require('shortid');

var soumissionSchema = new Schema({
  answers: [{
    _id: {
      type: Schema.Types.ObjectId,
      ref: 'Question',
      require: true
    },
    title: {
      type: String,
      require: true
    },
    type: {
      type: String,
      require: true
    },
    answer: {
      type: {},
      require: true
    }
  }],
  questionnaire: {
    type: Schema.Types.ObjectId,
    ref: 'Questionnaire',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  submitter: {
    type: String,
    default: 'Anonyme'
  }
});

var soumission = mongoose.model('Soumission', soumissionSchema);

module.exports = soumission;