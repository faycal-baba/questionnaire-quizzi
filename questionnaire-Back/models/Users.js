const mongoose = require('mongoose');
var crypto = require('crypto'),
    jwt = require('jsonwebtoken');


      //_variables = require('../environment.variable')
      Schema = mongoose.Schema;


let User = new Schema({
    name : {
        type : String,
        required : true
    },
    email : {
        type : String,
        unique : true,
        required : true
    },
    birthdate : {
        type : Date,
        required : true
    },
    hash: String,
    salt: String
});

User.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

User.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
  };

  User.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
  
    return jwt.sign({
      _id: this._id,
      email: this.email,
      name: this.name,
      birthdate: this.birthdate,
      exp: parseInt(expiry.getTime() / 1000),
    },"MohaMoha"/*_variables.SECRET*/); // DO NOT KEEP YOUR SECRET IN THE CODE!
  };



mongoose.model('User', User);