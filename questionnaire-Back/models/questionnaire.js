const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Soumission = require('./soumission.js'),
  shortId = require('shortid');


let Questionnaire = new Schema({
  
  title: {
    type: String,
    trim: true,
    required: true
  },
  submittedBy: {
    type: String
  },
  submittedById: {
    type: String
  },
  description: {
    type: String,
    default: ''
  },
  dateCreate: {
    type: Date
  },
  dateLimite:{
    type: Date
  },
  questions: [{
    type: Schema.Types.ObjectId,
    ref: 'Question'
  }],
  etat: {
    type: String,
    enum : ['Opened','Shared','Closed'],
    default: 'Opened'
  }
});

Questionnaire.statics.findAllQuestions = function
  findAllQuestions(surveyId, func) {
  this.findById({
    _id: surveyId
  }).populate('questions').exec(func);
}

Questionnaire.statics.getResponseCount = function
  getResponseCount(surveyId, func) {
  Soumission.find({})
    .where('questionnaire')
    .equals(surveyId)
    .exec(func);
};

Questionnaire.statics.getResponses = function
  getResponses(surveyId, func) {
  Soumission.find({})
    .where('questionnaire')
    .equals(surveyId)
    .populate('soumission.question')
    .exec(func);
};

module.exports = mongoose.model('Questionnaire', Questionnaire);
