const
  Questionnaire = require('../models/questionnaire.js'),
  Question = require('../models/question.js'),
  express = require('express'),
  validator = require('express-form'),
  questionRoutes = express.Router(),
  field = validator.field;


questionRoutes.post('/:id',
  function (req, rep) {
    var id = req.param.id;
    var data = req.body;
    data.choices = data.choices.split(',');
    Question.create(data, function (error, question) {
      if (error) {
        rep.send(error);
      } else {
        Questionnaire.update({
          _id: id
        }, {
            $push: {
              questions: question
            }
          }, {
            safe: true,
            upsert: true
          }, function (error, questionnaire) {
            if (error) {
              console.log(error);
            }
            rep.redirect('/admin/questionnaires/' + id);
          });
      }
    });
  });

questionRoutes.put('/:id', function (req, rep) {
  var data = req.body;
  data.choices = data.choices.split(',');
  var id = req.params.id;
  Question.update({
    _id: id
  }, data, function (error, numAffected, res) {
    if (error) {
      rep.send('Erreur pendant la modification de la question');
    } else {
      rep.send(res);
    }
  });
});

questionRoutes.delete('/:id', function (req, rep) {
  var id = req.params.id;
  Question.remove({
    _id: id
  }, function (error) {
    if (error) {
      rep.send('Erreur pendant la suppression de la question');
    } else {
      rep.send('Deleted');
    }
  });
});

questionRoutes.get('/:id', function (request, response) {
  var id = request.params.id;
  console.log("questionRoutes.get " + id);

  Questionnaire.findAllQuestions(id, function (err, results) {
    if (err) response.json(err);
    if (results) {
      response.json(results.questions);
    }
    else {
      response.json({ "message": "pas de liste de questions" });
      console.log("pas de liste de questions");
    }

  });
});

module.exports = questionRoutes;