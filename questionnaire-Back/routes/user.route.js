const express = require('express'),
    router = express.Router();

let User = require('../models/Users');

var jwt = require('express-jwt');
var auth = jwt({
    secret: 'MohaMoha',
    userProperty: 'payload'
});

var ctrlProfile = require('../controllers/profile');
var ctrlAuth = require('../controllers/authentication');



router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);
router.get('/profile', auth, ctrlProfile.profileRead);

//Get the user Model







/* router.route('/register').post(function (request, response) {
    let user = new User(request.body);
    console.log("request received");
    console.log(request.body);
    user.save()
        .then(game => {
            response.status(200).json({
                'User': 'User in added successfully'
            });
        })
        .catch(err => {
            response.status(400).send("unable to save to database");
        });
}); */





module.exports = router;